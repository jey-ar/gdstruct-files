#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>
#include <assert.h>

using namespace std;

//Replication attempt of all bugs/glitches and appearance as observed in the sample program.

//Remove index from both arrays
void RemoveIndex(UnorderedArray<int> &unordered, OrderedArray<int> &ordered) {
	cout << endl; //an extra line break here

	cout << "\nInput index to remove: ";
	int input;
	cin >> input;
	//Inputing a char here crashes the program

	//unordered
	if (input > unordered.getSize() - 1) {
		cout << "Index is more than the current size!" << endl;
	}
	else {
		unordered.remove(input);
	}
	//ordered
	if (input > ordered.getSize() - 1) {
		cout << "Index is more than the current size!" << endl;
	}
	else {
		ordered.remove(input);
	}

	cout << "\nElement Removed: " << input << endl;
	cout << "Unordered contents: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << " ";

	cout << "\nOrdered contents:   ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << " ";
	cout << endl;
	return;
}

//Search an element from both arrays based on input
void SearchElement(UnorderedArray<int> &unordered, OrderedArray<int> &ordered, int input) {
	int linearResult = unordered.linearSearch(input);
	int binaryResult = ordered.binarySearch(input);

	cin.clear(); 

	if (linearResult >= 0) {
		cout << endl;
		cout << "Linear Search took " << unordered.getCounter() << " comparisons." << endl;
		cout << "Binary Search took " << ordered.getCounter() << " comparisons." << endl;

		cout << "Element " << input << " found. " 
			<< "Index " << unordered.linearSearch(input) << " for UnorderedArray, "
			<< "index " << ordered.binarySearch(input) << " for OrderedArray." << endl;
	}
	else {
		cout << "Element " << input << " not found." << endl;
	}
	cout << endl;
	return;
}

//Expand and generate new elements from both arrays based on input
void ExpandAndGenerate(UnorderedArray<int> &unordered, OrderedArray<int> &ordered, int input) {
	if (input > 0) {
		for (int i = 0; i < input; i++)
		{
			int rng = rand() % 101;
			unordered.push(rng);
			ordered.push(rng);
		}
	}
	cin.clear();

	//Will show regardless of input
	cout << "\nArrays have been expanded:" << endl;
	cout << "Unordered contents: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << " ";

	cout << "\nOrdered contents:   ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << " ";
	cout << endl;
	return;
}


int main() {
	srand(time(NULL));
	cout << "Enter size for dynamic arrays: ";
	int size;
	cin >> size;

	//throws error when in the negatives
	assert(size >= 0); 

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	while (true) {
		system("cls");

		cout << "Unordered contents: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << " ";
		cout << "\nOrdered contents:   ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << " ";

		cout << "\n\nWhat do you want to do?"
			<< "\n1 - Remove element at index"
			<< "\n2 - Search for element"
			<< "\n3 - Expand and generate random values" << endl;
		int choice;
		cin >> choice;
		//a char locks the program

		int input = NULL;
		switch (choice) {
		case 1:
			RemoveIndex(unordered, ordered);
			break;
		case 2:
			cout << "\nInput element to search: ";
			cin >> input;
			//a char locks the program
			SearchElement(unordered, ordered, input);
			break;
		case 3:
			cout << "\nInput size of expansion: ";
			int input;
			cin >> input;
			//a char locks the program
			ExpandAndGenerate(unordered, ordered, input);
			break;
		default:
			cout << endl;
			break;
		}

		system("pause");
	}
	return 0;
}