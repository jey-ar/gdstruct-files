// Seatwork-03-Input-Guild.cpp 

#include <iostream>
#include <string>

using namespace std;

//Member struct
struct Member {
	string username;
};

//Guild struct
struct Guild {
	string name; //Guild name
	Member* members; //Dynamic member array
	int numElements; //Number of elements
	int maxSize; //Max size

	//Set name of guild
	void InputName() {
		cout << "Input name of guild: ";
		cin >> name;
	}

	//Only used for intial size
	void SetSize() {
		cout << "Input size of the guild: ";
		cin >> maxSize;
		members = new Member[maxSize];
		numElements = 0;
		return;
	}

	//Add a member and resize guild if numElements exceed maxSize
	void AddMember(Member member) {		

		if (numElements >= maxSize) {
			Member* tempMembers = new Member[maxSize + 1];

			//Copy members of old to new but using the oldSize
			for (int i = 0; i < maxSize; i++) {
				tempMembers[i] = members[i];
			}

			//Delete old array
			delete[] members;

			//make temp array the array
			members = tempMembers;
			maxSize++;
		}

		members[numElements] = member;		
		numElements++;
		return;
	}

	void RenameMember() {
		int index;
		cout << "Input the index of the member to rename: ";
		cin >> index;

		if (index < 0 || index > maxSize) {
			cout << "Invalid command!" << endl;
			system("pause");
			return;
		}

		index--;
		string newName;

		cout << "Input new name of " << members[index].username << ": " << endl;
		cin >> newName;

		members[index].username = newName;
		cout << "Member is renamed!" << endl;
		return;
	}

	//Delete a member and resize the whole guild
	void DeleteMember() {
		int index;
		cout << "Input the index of the member to delete: " << endl;
		cin >> index;

		if (index < 0 || index > maxSize) {
			cout << "Invalid command!" << endl;
			system("pause");
			return;
		}

		int newSize = maxSize - 1;
		Member* tempMembers = new Member[newSize];
		//Shift members of old array
		for (int i = index - 1; i < newSize; i++) {
			members[i] = members[i + 1];
		}

		//Copy members of old to new
		for (int i = 0; i < newSize; i++) {
			tempMembers[i] = members[i];
		}

		//Delete old array
		delete[] members;

		//tempArray becomes permArray
		members = tempMembers;
		numElements--;
		maxSize = newSize;
		return;
	}

	//Print all members
	void PrintAll() {
		for (int i = 0; i < numElements; i++) {
			cout << "[" << i + 1 << "] - " << members[i].username << endl;
		}
		return;
	}
};


int main()
{
	bool guildCreated = false;

	cout << "Welcome! \n" 
		<< "Please carefully follow the instructions to successfully create your guild." << endl;
	Guild newGuild;

	newGuild.InputName();
	newGuild.SetSize();

	for (int i = 0; i < newGuild.maxSize; i++) {
		string memberName;
		cout << "Input name of Member #" << i + 1 << ": ";
		cin >> memberName;
		newGuild.AddMember({ memberName });
	}

	guildCreated = true;

	cout << "Congratulations! Your guild was hopefully created successfully." << endl;

	system("pause");
	system("cls");

	while (guildCreated) {
		system("cls");
		string newMemberName;
		int command = 0;
		cout << "========================" << endl;
		cout << "Guild Management System" << endl;
		cout << "========================" << endl;
		cout << "Name: " << newGuild.name << endl;
		cout << "Size: " << newGuild.maxSize << endl;
		cout << "========================" << endl;
		cout << "[1] - Display all Members" << endl;
		cout << "[2] - Rename a Member" << endl;
		cout << "[3] - Add a new Member" << endl;
		cout << "[4] - Remove a Member" << endl;
		cout << "Type the number to perform the operation: ";
		cin >> command;

		system("cls");

		switch (command) {
		case 1:
			cout << "========================" << endl;
			cout << "Display all Members" << endl;
			cout << "========================" << endl;
			newGuild.PrintAll();
			system("pause");
			break;
		case 2:
			cout << "========================" << endl;
			cout << "Rename a Member" << endl;
			cout << "========================" << endl;
			newGuild.PrintAll();
			cout << endl;
			newGuild.RenameMember();
			system("pause");
			break;
		case 3:		
			cout << "========================" << endl;
			cout << "Rename a Member" << endl;
			cout << "========================" << endl;
			cout << "Input name of new Member: ";
			cin >> newMemberName;
			newGuild.AddMember({ newMemberName });
			system("pause");
			break;
		case 4:
			cout << "========================" << endl;
			cout << "Delete a Member" << endl;
			cout << "========================" << endl;
			newGuild.PrintAll();
			cout << endl;
			newGuild.DeleteMember();
			system("pause");
			break;
		default:
			cout << "Invalid Command!" << endl;
			system("pause");
			break;
		}
	}
	
	system("pause");
	return 0;
}