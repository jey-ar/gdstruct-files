#pragma once

#include <assert.h>


template<class T>
class UnorderedArray {
public:
	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0) {
		if (size) {
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);		
		}
	}

	virtual ~UnorderedArray() {
		if (mArray != NULL) {
			delete[] mArray;
			mArray = NULL;
		}
	}

	//Add element in the last position
	virtual void Push(T val) {
		//If array exists, we push a value
		assert(mArray != NULL);
		
		if (mNumElements >= mMaxSize) {
			Expand();
		}

		//Add value to last position
		mArray[mNumElements] = val;
		mNumElements++;
	}

	//Remove last element
	void Pop() {
		if (mNumElements > 0) mNumElements--;

		
	}
	
	//Remove an index 
	void Remove(int index) {
		assert(mArray != NULL);
		//If selected index is greater than or equal to mMaxSize
		if (index >= mMaxSize) return;

		//Shift elements from the right to the left
		for (int i = index; i < mMaxSize - 1; i++) {
			mArray[i] = mArray[i + 1];
		}

		//Reduce number of elements
		mNumElements--;
		
		//if elements is greater than Max Size
		if (mNumElements >= mMaxSize) {
			mNumElements = mMaxSize;
		}
	}

	//Modify [] operator
	virtual T& operator[](int index) {
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	//Return number of elements
	int GetSize() {
		return mNumElements;
	}

	//Return maxSize;
	int GetMaxSize() {
		return mMaxSize;
	}

	//Find a value
	int Find(T val) {
		assert(mArray != NULL);

		//return index
		for (int i = 0; i < GetSize() ; i++) {
			if (mArray[i] == val) return i;
		}

		//If nothing
		return -1;
	}

private:
	T* mArray;
	int mMaxSize;
	int mGrowSize;
	int mNumElements;

	bool Expand() {
		if (mGrowSize <= 0) return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}
};