// VectorTutorial.cpp 

#include <string>
#include <iostream>
#include "UnorderedArray.h"

using namespace std;

int main()
{
	UnorderedArray<int> grades(20);
	for (int i = 0; i < 10; i++) {
		grades.Push(i);
	}


	cout << "Ini values:" << endl;
	for (int i = 0; i < grades.GetSize(); i++) {
		cout << grades[i] << endl;
	}

	cout << "Remove index 1" << endl;
	grades.Remove(1);

	cout << "After removing:" << endl;
	for (int i = 0; i < grades.GetSize(); i++) {
		cout << grades[i] << endl;
	}
	
	grades.Push(50);

	cout << "Find value 50" << endl;
	cout << grades.Find(50) << endl;

	cout << "Find 999" << endl;
	cout << grades.Find(999) << endl;
	system("pause");
    return 0;
}

